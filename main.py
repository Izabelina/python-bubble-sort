def bubble_sort(list):
    n = len(list)
    while n > 1:
        for i in range(0, n - 1):
            if list[i] > list[i+1]:
                 list[i], list[i+1] = list[i+1], list[i]
        n -= 1
        print(list)
    return list

"""
+ test0 = lista[0] - sprawdza co się dzieje przy wyszukiwaiu na pustej liście
+ test_nie_ma = lista[90](...) - gdy nie ma wyszukiwanego elementu
+, - test_wiele - pojawi się kilka razy
+ test_tylko - pojawią się tylko elementy, które chcę wyszukać
+ test_ok -  pojawi się tylko raz
"""

def find(list, num):
    found = []
    found = found.append(num)
    begin = 0
    end = len(list)
    middle = (begin + end) // 2
    if len(list) != 0:        
        while begin <= end:
            found = []
            found.append(num)
            middle = (begin + end) // 2
            occur = list.count(num)
            if occur == 0:
                print(f"Liczba {num} nie występuje na liście")
                break
            elif list[middle] == num:
                print(f"Liczba {num} wystąpiła {occur} razy na liście")
                return middle, found
            elif list[middle] > num:
                end = middle - 1 
            else:
                begin = middle + 1
                print(f"Liczba {num} wystąpiła {occur} razy na liście")
                return middle + 1, found
    else:
        print("Ta lista jest pusta")

                  
                  
