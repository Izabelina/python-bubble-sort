from pathlib import Path
import csv



file_path = Path.home()/"PYTHON"/"bubble-sort"/"wig_d.csv"
print(file_path)

lista = []

with file_path.open(mode="r", encoding="utf-8") as file:
    reader = csv.reader(file)
    for row in reader:
        # Convert row to list of integers
        int_row = [int(value) for value in row]
        # Append the list of integers to daily_temperatures list
        lista.append(int_row)
